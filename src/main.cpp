/********************************************
 －－－－ESP32 Blink Led DEMO－－－－
  File name：Blink.ino
  Version：V1.0
  Illustrate：Hello World experiment
 ********************************************/
#include <Arduino.h>

#define RED_LED   4
#define GREEN_LED 16
#define BLUE_LED  17
#define DELAY     400

void setup() {
  // Инициализация последовательного порта
  Serial.begin(115200);
  Serial.println("Start Serial");
  // Режим работы порта 
  pinMode(RED_LED, OUTPUT);
  // Устанавливаем значание HIGH (напряжение на порту IO4 - 3.3 вольт)
  digitalWrite(RED_LED, HIGH);
  // Режим работы порта 
  pinMode(GREEN_LED, OUTPUT);
  // Устанавливаем значание HIGH (напряжение на порту IO16 - 3.3 вольт)
  digitalWrite(GREEN_LED, HIGH);
    // Режим работы порта 
  pinMode(BLUE_LED, OUTPUT);
  // Устанавливаем значание HIGH (напряжение на порту IO17 - 3.3 вольт)
  digitalWrite(BLUE_LED, HIGH);
}

void loop(){
  Serial.println("Зажигаем красный светодиод на порту IO4");
  // Устанавливаем значение LOW (напряжение на порту IO4 - 0 вольт)
  digitalWrite(RED_LED, LOW);
  // Задержка в мс
  delay(DELAY);
  Serial.println("Гасим красный светодиод на порту IO4");
  // Устанавливаем значение HIGH (напряжение на порту IO4 - 3.3 вольт)
  digitalWrite(RED_LED, HIGH);
  // Задержка в мс
  delay(DELAY);
    Serial.println("Зажигаем зеленый светодиод на порту IO16");
  // Устанавливаем значение LOW (напряжение на порту IO16 - 0 вольт)
  digitalWrite(GREEN_LED, LOW);
  // Задержка в мс
  delay(DELAY);
  Serial.println("Гасим зеленый светодиод на порту IO16");
  // Устанавливаем значение HIGH (напряжение на порту IO16 - 3.3 вольт)
  digitalWrite(GREEN_LED, HIGH);
  // Задержка в мс
  delay(DELAY);
      Serial.println("Зажигаем синий светодиод на порту IO17");
  // Устанавливаем значение LOW (напряжение на порту IO17 - 0 вольт)
  digitalWrite(BLUE_LED, LOW);
  // Задержка в мс
  delay(DELAY);
  Serial.println("Гасим синий светодиод на порту IO17");
  // Устанавливаем значение HIGH (напряжение на порту IO17 - 3.3 вольт)
  digitalWrite(BLUE_LED, HIGH);
  // Задержка в мс
  delay(DELAY);  
}